package main

import (
	"gocasbinlab/handler"
	"gocasbinlab/midw"
	"log"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
)

var (
	router *gin.Engine
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	enforcer, err := casbin.NewSyncedEnforcer("resources/rbac_model.conf", "resources/policy.csv")
	if err != nil {
		log.Fatalf("failed to intitialize enforcer: %s", err)
	}
	// enforcer.StartAutoLoadPolicy(10 * time.Second)

	accessControl := midw.NewAccessControl(enforcer)
	router = gin.Default()
	resource := router.Group("/api")
	resource.Use(midw.AuthHandler, accessControl.GetAuthorizer())
	{
		resource.Any("/*resource", handler.EchoResource)
	}

	casbinRouter := router.Group("/casbin")
	{
		casbinRouter.GET("/subjects", func(c *gin.Context) {
			c.JSON(200, enforcer.GetAllSubjects())
		})

		casbinRouter.GET("/policies", func(c *gin.Context) {
			c.JSON(200, enforcer.GetNamedPolicy("p"))
		})

		casbinRouter.GET("/roles", func(c *gin.Context) {
			c.JSON(200, enforcer.GetAllRoles())
		})
	}
}

func main() {
	log.Println("listening on :8080")
	_ = router.Run(":8080")
}
