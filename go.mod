module gocasbinlab

go 1.14

require (
	github.com/casbin/casbin/v2 v2.6.8
	github.com/gin-gonic/gin v1.6.3
)
