# Casbin example

This example demonstrates the way to use Casbin authorized resources.

## Get started

Run application, send request with assumed username in `x-user` header.

- user `x` with role `sys` has access to any routes
- user `a` with role `admin` has access to `/api/resources/*`

...
